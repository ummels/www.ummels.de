#!/bin/sh
convert="convert -auto-orient -interlace Plane -quality 85"
for file in "$@"; do
	filename=$(basename "$file")
	extension=$(echo "${filename##*.}" | tr '[:upper:]' '[:lower:]')
	name="${filename%.*}"
	$convert -resize 1280 "$file" "$name.$extension"
done
