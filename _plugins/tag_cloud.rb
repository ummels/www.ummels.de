# taken from https://gist.github.com/yeban/2290195
module Jekyll
  class TagCloud < Liquid::Tag
    safe = true

    def initialize(tag_name, params, tokens)
      super
    end

    def render(context)
      site = context.registers[:site]
      
      # get an Array of [tag name, tag count] pairs
      count = site.tags.map do |name, posts|
        [name, posts.count]
      end

      # clear nils if any
      count.compact!
      
      # sort by name
      count.sort_by(&:first)

      # get the minimum, and maximum tag count
      min, max = count.map(&:last).minmax
      
      # map: [[tag name, tag count]] -> [[tag name, tag weight]]
      weight = count.map do |name, count|
        # logarithmic distribution
        weight = (Math.log(count) - Math.log(min))/(Math.log(max) - Math.log(min))
        [name, weight]
      end
      
      # reduce the Array of [tag name, tag weight] pairs to HTML
      weight.reduce("") do |html, tag|
        name, weight = tag
        size = 90 + (60 * weight).to_f
        html << "<a style='font-size: #{size}%' href='/tags/#{name}'>#{name}</a>\n"
      end
    end
  end
end

Liquid::Template.register_tag('tag_cloud', Jekyll::TagCloud)
