---
date: 2016-01-31 18:26:00+01:00
layout: single
slug: going-static
title: Going Static
lang: en
tags:
- Design
- Blogging
---

After almost 10 years, I have decided to ditch [Wordpress][] for [Jekyll][], a _static blog engine_. As opposed to Wordpress, Jekyll is not dependent on [MySQL][] and [PHP][], but simply generates [static][] HTML files from sources in [Markdown][], so you can create your blog posts comfortably using your favourite text editor, let Jekyll generate the HTML and put it on an arbitrary web server. As the server only needs to serve the HTML pages, the attack surface is greatly reduced.

Like Wordpress, Jekyll boasts a huge number of themes, of which I have chosen the excellent [Minimal Mistakes][] theme in order to create a fresh but familiar look for this blog.

Since many image links in older posts were broken due to an outdated Gallery installation, I have decided to only convert posts from 2014 or later. Moreover, while I was able to copy the comments from these posts, I have not yet decided how to let people submit new comments since this requires at least some dynamic server code. Hence, at the moment it is not possible to post comments on this blog.

After restoring comment support, I am planning to move the blog over to <del>GitHub Pages and CloudFlare</del> [GitLab Pages][glpages], so I can finally serve this site over HTTPS. For now you can already browse the [complete source code][sources] on <del>GitHub</del> GitLab.

[Wordpress]: https://wordpress.org
[Jekyll]: https://jekyllrb.com
[static]: https://en.wikipedia.org/wiki/Static_web_page
[MySQL]: https://www.mysql.com
[PHP]: https://secure.php.net
[Markdown]: https://en.wikipedia.org/wiki/Markdown
[Minimal Mistakes]: https://mmistakes.github.io/minimal-mistakes/
[glpages]: https://about.gitlab.com/features/pages/
[sources]: https://gitlab.com/ummels/www.ummels.de
