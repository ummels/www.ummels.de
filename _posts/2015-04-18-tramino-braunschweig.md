---
date: 2015-04-18 15:48:45+00:00
layout: single
slug: tramino-braunschweig
title: Tramino Braunschweig
wordpress_id: 582
lang: en
tags:
- Straßenbahn
---

After some delays due to certification, the new [Tramino](https://www.verkehr-bs.de/unternehmen/tramino-stadtbahnen.html) trams for Braunschweig have finally entered into service last Monday. The first two pictures show unit 1451 running on line M5.

{% image P4135368.jpg alt="Tramino Braunschweig Hauptbahnhof" %}

{% image P4135351.jpg alt="Tramino Braunschweig Ägidienkirche" %}

As can be seen on the next picture, the interior looks quite modern as well.

{% image P4135357.jpg alt="Tramino Braunschweig Interior" %}

Green lights above the doors indicate that the vehicle may be entered.

{% image P4135394.jpg alt="Tramino Braunschweig Heinrich-Büssing-Ring" %}

The next picture shows unit 1451 standing next to one of its ancestors built in 1995.

{% image P4135392.jpg alt="Tramino Braunschweig Heinrich-Büssing-Ring 2" %}

Finally, a bonus picture of a tram from the 2007 series coupled to a trailer built in 1974! These trailers will probably be retired as soon as enough new trams have entered into service.

{% image P4135395.jpg alt="Braunschweig Hauptbahnhof" %}
