---
date: 2014-06-04 15:03:37+00:00
layout: single
slug: back-to-blogging
title: Back to Blogging
wordpress_id: 371
lang: en
tags:
- Design
- Blogging
---

Okay, obviously this blog hasn't been very active for some time. Time to change that and get back to blogging, I guess. As you can see I've updated the [design](https://wordpress.org/themes/twentyfourteen) and [typography](https://www.google.com/fonts/specimen/Merriweather) of this blog, which should also lead to a good experience on mobile devices. (If not, please feel free to tell me in the comments). I already have some posts in mind, so check again soon.
