---
date: 2014-10-26 13:06:20+00:00
layout: single
slug: happy-birthday-shinkansen
title: Happy Birthday, Shinkansen!
wordpress_id: 442
lang: en
post_format:
- Image
tags:
- Bahn
- Shinkansen
---

We are celebrating the 50th birthday of the [Tōkaidō Shinkansen](https://en.wikipedia.org/wiki/Tōkaidō_Shinkansen) with a limited-edition [Tenugui](https://en.wikipedia.org/wiki/Tenugui), which was on sale in Nagoya this summer exclusively.

{% image PA264495-2.jpg alt="Shinkansen Tenugui" %}
